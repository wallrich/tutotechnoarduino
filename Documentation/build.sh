#! /bin/sh
export PATH="$HOME/.npm-global/bin:$PATH"
pandoc -F mermaid-filter -t revealjs -s -o tuto.html tuto.md -V revealjs-url=./reveal.js
pandoc -F mermaid-filter -o tuto.pdf tuto.md
