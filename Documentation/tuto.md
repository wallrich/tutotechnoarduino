---
title: ARDUINO 
tags: tutotechno, arduino, C++
date: June, 06 2023
theme: league
margin: 0.3

---

## Disclaimer

### why this tutorial ?

* Professional -> robotic
* At home
  * DIY
  * LOW TECH
  * Teaching ?

### Download

```bash
git clone git@gitlab.inria.fr:wallrich/tutotechnoarduino.git
git clone https://gitlab.inria.fr/wallrich/tutotechnoarduino.git
```

Or download as Zip.

## Arduino Board

### What is arduino

Arduino is TM for an open-source electronics platform based on easy-to-use hardware and software. It's intended for anyone making interactive projects.

* Inexpensive
* Cross-platform
* Simple, clear programming environment (based on C++)
* Open source and extensible software
* Open source and extensible hardware

[http://arduino.cc](http://arduino.cc)

### Cards

A lot of official cards

| Cards | prices |
|--|--|
| Arduino Uno rev3 | 22 € |
| Arduino Uno R4 Minima | 18 € |
| Arduino Uno R4 Wifi | 25 € |
| Arduino Nano | 21 € |
| Arduino Micro | 21 € |
| Arduino Zero | 38 € |
| ... | ... |

And so much clones (cheaper)...

### Arduino Uno R3 1/2

![Arduino uno](images/arduino-uno2.png)

### Arduino Uno R3 2/2

|pins| descriptions |
|-|-|
| Digital pins | used read or write boolean (HIGH / LOW) |
| Analog pins | used read or write values (0 - >1023) |
| ISCP (ISP) | flash bootloader |
| Vin | power supplier |
| AREF | Analog reference |

### ATmega328p

8-bit AVR® RISC-based microcontroller

* 32 KB ISP Flash memory
* 1024B EEPROM
* 2  KB SRAM
* 23 general purpose I/O lines
* 32 general purpose working registers

(with arduino bootloader already in)

### Sensors

![pulse](images/sensor.png)

(humidity, electricity, temperature, movement, ultrasonic, rain, light, microphone, hall effect, acceleration, fire, impact, line tracker, gaz, camera, ...)

### Actuators

![relay](images/actuators.png)

(Relays, Servo motors, step motors, fans, vibration, leds, lcd displays, touchscreens , ZigBee z-wave (z-uno), GSM, bluetooth, NFC, ...)

### Shields

![NFC](images/shield.png)

(Same as previous)

## The tutorial

### What we will do

1. Blink (the "Hello Word" of arduino)
1. Use serial to send messages
1. Mesure Temp sensor ds18b20
1. Use lcd crystal screen 16x2 i2c
1. Add serial link between 2 arduinos

### What we will NOT do

* welding
* diagrams ([fritzing](https://fritzing.org/))
* ISCP
* ...

## Tutorial 0 : Blink-Serial

### Blink

On your PC :

1. install the IDE
[https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)

1. Connect the arduino to the PC (USB)
1. File -> Example -> 01.Basic -> Blink
1. find the good serial port and upload

And look at blink...

### The code 1/2

```C
void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
```

[https://www.arduino.cc/reference/en/](https://www.arduino.cc/reference/en/)

### The code 2/2

```C
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

[https://www.arduino.cc/reference/en/](https://www.arduino.cc/reference/en/)

### Serial

In the arduino IDE

```
Tools -> Serial Monitor
```

And the code 

```C
void setup() {
  Serial.begin(9600);
  Serial.print("Hello TTY !");
}

void loop() {
  Serial.print("A debug message");
  delay(1000);
}
```

[https://www.arduino.cc/reference/en/](https://www.arduino.cc/reference/en/)


### And now... Code !

1. Do a loop of write on the serial port
1. Check on the PC Side :  **Outils** -> **Moniteur serie**

![](https://media.giphy.com/media/ySpxjJmsq9gsw/giphy.gif)



## Tutorial 1 : DS18S20

### Objectif

Take a temperature mesure from a sensor, and display the value on the serial port.


### Test board

This is the way to test circuits

![test board](images/boardexplanation_bb.png)


### DS18S20 Sensor

Temperature sensor 1-wire

![ds18s20](images/ds18s20.jpeg)

* Measures Temperatures from -55°C to +125°C (-67°F to +257°F)
* ±0.5°C Accuracy from -10°C to +85°C 
* 9-Bit Resolution

### How it works

Each temperature sensor DS18B20 has it own 64bits serial code (uniq ID) => you can have more than one sensor (bus)

A 1-Wire bus is made by three wires

* ground,
* power supply (5 volts)
* data wire. 

*Un seul composant externe est nécessaire pour faire fonctionner un bus 1-Wire : une simple résistance de 4.7K ohms en résistance de tirage à l'alimentation sur la broche de données.*

### Tuto

* Cabler le capteur de température & sa résistance
* Installer les bibliothèques (OnWire, *DallasTemperature*)
* Programmer la mesure d'une température et affichage sur le port Serie.

### Resistances

Comment trouver la bonne résistance ?

![resistor colors](images/resistor-color-chart.png)

### Cablage

![cablageds18s20](images/cablageds18b20.png)

### Périphériques 1-Wire

Un "scratchpad" = de mémoire tampon sécurisée ou l’on peut venir lire et / ou écrire des données.

Le scratchpad du capteur DS12B20 est divisé en quatre parties :

* Le résultat de la dernière mesure de température (deux octets),
* Deux octets à usages divers
* Le registre de configuration du capteur,
* Une somme de contrôle.

### Scratchpad

![scratchpad](images/scratchpad_ds18b20.jpg)

```C++
temperature_celsius = (int16_t) ((MSB << 8) + LSB) * 0.0625. 
```

### Et maintenant, codez

1. Chercher un peu sur le net
1. Installez les bibliothèques **Outil** -> **Gérer les bibliothèques**
  (OneWire, DallasTemperature (option))
1. Lire la température sur le capteur
1. Ecrire la température sur le port serie

![](https://media.giphy.com/media/KpACNEh8jXK2Q/giphy.gif)




## Tutorial 2 : LCD Screen
 
### Objectif

Mesurer la température d'un capteur et l'afficher sur l'écran LCD.

### lcd crystal screen 16x2

![Lcd 16x2](images/lcd_photo.png)


### lcd crystal screen 16x2 (non ic2)

![Lcd base](images/LCD_Base_.png)

### The circuit

Use "LiquidCrystal"

 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K potentiometer:
   * ends to +5V and ground
   * wiper to LCD VO pin (pin 3)
   * 10K poterntiometer on pin A0


### Bus I2C (Inter Integrated Circuit Bus) 1/3

![I2C](images/bus_I2C.jpg)

* Vitesse de transfert : 100 kbit/s
* Fast Mode à 400 kbit/s
* Fast Mode plus (FM+) à 1 Mbit/s.
* High Speed à 3,4 Mbit/s.
* Ultra Fast mode à 5 Mbit/s

### Bus I2C (Inter Integrated Circuit Bus) 2/3

* Deux lignes (bidirectionnelles) nécessaires,
  * données - SDA -
  * horloge de synchronisation - SCK - (1 bit échangé à chaque « coup » d'horloge)
* transmission synchrone. le périphérique maître (master) génère le signal d'horloge qui synchronise et cadence les échanges

### Bus I2C (Inter Integrated Circuit Bus) 3/3

* la relation entre les périphériques du bus est de type maître-esclave (master/slave). Le maître est à l'initiative de la transmission et s'adresse à un esclave (ou tous les esclaves)
* chaque périphérique sur le bus I2C est adressable, avec une adresse unique pour chaque périphérique du bus
* l'I2C gère le fonctionnement multimaître (multi-master), plusieurs périphériques maîtres peuvent prendre simultanément le contrôle du bus (système d'arbitrage des maîtres et gestion des collisions).

### Tuto

* Cabler l'affichage sur bus I2C
* Afficher la température sur l'écran
* Rajouter le caractère spécial "°" à l'affichage

### Cablage

![Cablage](images/AfficheurLCD-arduino.png)

<div style="font-size:80%" >
*Pensez à régler le rétroéclairage*
</div>

### Installation

Installer "LiquidCrystal_I2C"

```C++
  // définit le type d'écran lcd 16 x 2
  LiquidCrystal_I2C LCD(0x27,16,2); 

   LCD.init(); // initialisation de l'afficheur
   LCD.backlight();
   
   LCD.setCursor(1, 0);
   LCD.print("HELLO WORD");
```

<div style="font-size:80%" >
[https://www.arduino.cc/reference/en/libraries/liquidcrystal-i2c/](https://www.arduino.cc/reference/en/libraries/liquidcrystal-i2c/)
</div>

### Et maintenant, codez !

1. Afficher la température
1. Rajouter le caractère "°" 

![](https://media.giphy.com/media/pFwRzOLfuGHok/giphy.gif)

```C++
// Tips
LCD.createChar()
```

## Tutorial 3 : 2 arduinos

### Objectif

* Mesurer la température d'un capteur et l'afficher sur l'écran LCD, 
* Transmettre la mesure à un autre arduino (autre groupe) en RS323
* Lire la mesure de l'autre arduino et l'afficher sur l'écran LCD

<div style="font-size:80%" >
*On va faire utiliser les ports digitaux 3 & 4 (car 0 & 1 sont utilisés sur l'USB)*
</div>



### Cablage

![RS232 Serial](images/ardRS232.png)


### Installation

Installer [SoftwareSerial](https://docs.arduino.cc/learn/built-in-libraries/software-serial
)

```C++
#include <SoftwareSerial.h>
#define rxPin 3
#define txPin 4
// Set up a new SoftwareSerial object
SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

void setup()  {
    // Define pin modes for TX and RX
    pinMode(rxPin, INPUT);
    pinMode(txPin, OUTPUT);

    // Set the baud rate for the SoftwareSerial object
    mySerial.begin(9600);
}
```
----

```C++
void loop() {
    if (mySerial.available() > 0) {
        char c = mySerial.read();
    }
}
```


### Et maintenant, codez !

1. Faites la liaison RS323
1. Codez la lecture et l'ecriture 

![](https://media.giphy.com/media/T8Dhl1KPyzRqU/giphy.gif)

```C++
// Tips : N'écrivez sur le port serie
// que si la température a changée.
```

## Conclusion

### Have fun

![](https://media.giphy.com/media/tK5JkmMAPveNO/giphy.gif)

See you soon for another TutoTechno