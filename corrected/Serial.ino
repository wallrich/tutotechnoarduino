/*
  Serial
*/

#include "Wire.h"

int i = 0;
byte status;

void setup()
{
  Serial.begin(9600);
  Serial.println("Hello !");
}

// the loop function runs over and over again forever
void loop()
{

  /* Affiche la température */
    Serial.println("Number ");
    Serial.print(i);
    Serial.println();
    i++;

    delay(1000);
}
