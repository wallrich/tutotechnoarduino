/*
  Serial
*/

#include "Wire.h"
#include "LiquidCrystal_I2C.h"

LiquidCrystal_I2C LCD(0x27, 16, 2); // définit le type d'écran lcd 16 x 2

#include <OneWire.h>

/* Broche du bus 1-Wire */
const byte BROCHE_ONEWIRE = 4;

enum DS18B20_RCODES
{
  READ_OK,         // Lecture ok
  NO_SENSOR_FOUND, // Pas de capteur
  INVALID_ADDRESS, // Adresse reçue invalide
  INVALID_SENSOR   // Capteur invalide (pas un DS18B20)
};

#include <SoftwareSerial.h>
#define rxPin 0
#define txPin 1
SoftwareSerial otherSerial(rxPin, txPin);

byte simvol[8] = {
    0b01100,
    0b10010,
    0b10010,
    0b01100,
    0b00000,
    0b00000,
    0b00000,
    0b00000};

OneWire ds(BROCHE_ONEWIRE);

float t = 0.0;
float oldTemp = 1.0;
byte status;

byte getTemperature(float *temperature, byte reset_search)
{
  byte data[9], addr[8];
  // data[] : Données lues depuis le scratchpad
  // addr[] : Adresse du module 1-Wire détecté

  /* Reset le bus 1-Wire ci nécessaire (requis pour la lecture du premier capteur) */
  if (reset_search)
  {
    ds.reset_search();
  }

  /* Recherche le prochain capteur 1-Wire disponible */
  if (!ds.search(addr))
  {
    // Pas de capteur
    return NO_SENSOR_FOUND;
  }

  /* Vérifie que l'adresse a été correctement reçue */
  if (OneWire::crc8(addr, 7) != addr[7])
  {
    // Adresse invalide
    return INVALID_ADDRESS;
  }

  /* Vérifie qu'il s'agit bien d'un DS18B20 */
  if (addr[0] != 0x28)
  {
    // Mauvais type de capteur
    return INVALID_SENSOR;
  }

  /* Reset le bus 1-Wire et sélectionne le capteur */
  ds.reset();
  ds.select(addr);

  /* Lance une prise de mesure de température et attend la fin de la mesure */
  ds.write(0x44, 1);
  delay(800);

  /* Reset le bus 1-Wire, sélectionne le capteur et envoie une demande de lecture du scratchpad */
  ds.reset();
  ds.select(addr);
  ds.write(0xBE);

  /* Lecture du scratchpad */
  for (byte i = 0; i < 9; i++)
  {
    data[i] = ds.read();
  }

  /* Calcul de la température en degré Celsius */
  *temperature = (int16_t)((data[1] << 8) | data[0]) * 0.0625;

  // Pas d'erreur
  return READ_OK;
}

void setup()
{

  // Define pin modes for TX and RX
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);

  // Set the baud rate for the SoftwareSerial object
  otherSerial.begin(9600);

  Serial.begin(9600);
  Serial.println("Hello TTY ds !");

  LCD.init(); // initialisation de l'afficheur
  LCD.backlight();

  LCD.createChar(1, simvol); // création du caractère personnalisé

  LCD.setCursor(1, 0);
  LCD.print("HELLO WORD");
}

// the loop function runs over and over again forever
void loop()
{
  Serial.print("DS18S20 take mesure ");

  if (getTemperature(&t, true) != READ_OK)
  {
    Serial.println(F("Erreur de lecture du capteur"));
    t = -100.0;
  }
  else
  {
    /* Affiche la température */
    Serial.print(F("Temperature : "));
    Serial.print(t, 2);
    Serial.write(176); // Caractère degré (bug in serial monitor)
    Serial.write('C');
    Serial.println();

    /* Affiche la température sur l'ecran LCD*/
    LCD.setCursor(0, 0);
    LCD.print("Temp : ");
    LCD.print(t, 2);
    LCD.print(char(1));
    LCD.print('C');
  }

  // Send the temperature on rs if new
  if (oldTemp != t)
  {
    oldTemp = t;
    otherSerial.println(t, 2);
  }

  // read temperature from other
  if (otherSerial.available() > 0)
  {
    LCD.setCursor(0, 1);
    LCD.print("OT : ");
    int i=0;
    while (otherSerial.available() > 0)
    {
      char c = otherSerial.read();
      if ( i < 8 ) {
            LCD.print(c);
      }
    }
  }
  else
  {
    LCD.setCursor(0, 1);
    LCD.print("No temp");
  }

  delay(1000); // wait for a second
}
